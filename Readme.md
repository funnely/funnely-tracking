# Funne.ly Event Tracking API #

The Funne.ly tracking API is used in your frontend so that we can track the behavior of your site visitors, populate your audiences and track the sales driven by your ads campaigns.

## 1. Installation ##

** Important: Please remove all Facebook events fired from your store before integrating Funne.ly. **

The Tracking API is written in Javascript, so in order to start, you will need to include a script in the <head> section of your site. Please request the url for this script to your account manager.

The script must be included in all your site pages, so for example, if you have different <head> sections for your products and for your categories, then you need to include the script in both places.

Here's an **example (don't copy from here)** on how including the script looks like:

![Screen Shot 2016-04-25 at 17.37.08.png](https://bitbucket.org/repo/RnG76r/images/320528116-Screen%20Shot%202016-04-25%20at%2017.37.08.png)

**Important**: The Funne.ly tracking API script is loaded asynchronously. This means that the script loads in parallel to other page assets (images, stylesheets, etc.). This makes the script loading not delay or interrupt the page loading time.

## 2. Setup ##

Once the script is included, you will need to call the tracking methods in the different sections of your site.

Different to the main script that you included in step 1, the following code snippets need to be included only in the sections where they are relevant. They should not be called in every page of your site. Inlcuding them at the end of the <body> section is a recommendation.

**Make sure to always enclose the code inside <script></script> tags**

### 2.1. Homepage Visit  ###

In order to track a homepage visit, you will need to add the following code in your homepage template.

```
#!javascript
  _funnely_q.push(['trackHomepageVisit'])

```

### 2.2. Product Category Visit ###

In order to track visits to a category product, you need to place the following code in the landing page for said category.


```
#!javascript
  _funnely_q.push(['trackCategoryVisit', {category_id: <category_id>, category_type: <category_type>}])
```

* The **category_type** variable is a string that represents a product categorization. For example, if you categorize your products by gender (Women, Men) but you also categorize them by Brand, you can set the category type in your gender category landing page to 'gender', and the category_type to 'brands' in your brands category landing page. This is very important if your different types of categories reside in different tables of your database.
* Notice that **<category_id>** should be the unique value that represents your category in your database. If you had a site where every category has its own page, then you should insert this code in every category page and replace the <category_id> by the id of each category.

On the other hand if you have a generic template for all your categories, you will only need to add this code once, and set your <category_id> to the id of the category that is currently being shown.

Here's an example in PHP of how this would work (of course this changes from implementation to implementation):


```
#!php
<?php
/**
 * The $category variable represents the model for the category that is being retrieved, 
 * and $category_model is  our ORM object.
 */
$category = $category_model->find_by_id($_REQUEST['category_id']);

# Include the common header for all the site's pages.
include('templates/header.php');
?>
<!-- Here's the content for your category template -->
<script type="text/javascript">
  _funnely_q.push(['trackCategoryVisit', {category_id: <?= $category->id ?>, category_type: 'default'}])
</script>
<?php
# Include the common footer for all the site's pages
include('templates/footer.php');
?>
```
### 2.3. Product Visit ###

In order to track visits to a product, you need to place the following code in the landing page for said product:

```
#!javascript
  _funnely_q.push(['trackProductVisit', {product_id: <product_id>, product_price: <product_price>, product_currency: <product_currency>}])

```
The *trackProductVisit* method takes three parameters:

* **product_id**: is the unique identifier of your product in your database.
* **product_price**: is a *float* amount that represents the price for the product
* **product_currency**: is a string representing the currency for the product price, expressed in ISO 4217 currency code (i.e. 'USD')

As with the previous steps, you can set these values in the template where you include this code, using your site's business logic. Here's an example in PHP:

```
#!php
<?php

/**
 * We assume that the $product variable represents the model for the product that is being retrieved, 
 * and $product_model is  our ORM object.
 */
$product = $product _model->find_by_id($_REQUEST['product _id']);

# Include the common header for all the site's pages.
include('templates/header.php');
?>
<!-- Here's your product page content -->
<script type="text/javascript">
  _funnely_q.push(['trackProductVisit', {product_id: <?= $product->id ?>, product_price: <?= $product->price ?>, product_currency: '<?= $product->currency ?>'}])
</script>
<?php
# Include the common footer for all the site's pages
include('templates/footer.php');
?>
```

### 2.4. Add to Cart Event Tracking ###

To track the 'Add to cart' event on your site, the following code snippet must be inserted in your 'Add to Cart' button *onclick* event.

Here's an example on how to do it, assuming your site uses jQuery.


```
#!html

<!-- This is your site's add to cart button -->
<button id="add_to_cart">Add to Cart</button>

<script type="text/javascript">
  $('#add_to_cart').click(function(e) {    
    _funnely_q.push(['trackAddToCart', {product_ids: [<product_id_1>, <product_id_2>, ...], product_price: <product_price>, product_currency: <product_currency>, content_name: <content_name>}]);
  });
</script>
```

If you already have an onclick event listener for your Add to cart button, then you can place the funnely.trackAddToCart() call in there.

The *trackAddToCart* function takes four parameters:

* **product_ids**: is an array of the unique identifiers of the products added to the cart. This field is mandatory.
* **product_price**: is a *float* amount that represents the price for the products added to the cart. This field is optional.
* **product_currency**: is a string representing the currency for the product price, expressed in ISO 4217 currency code (i.e. 'USD'). This field is optional.
* **content_name**: is a string with the name of the product added to the cart. This field is optional.

As with previous steps, you can replace these values using your site's business logic. Here's an example in PHP:

```
#!php
<?php

/**
 * We assume that the $product variable represents the model for the product that is being retrieved, 
 * and $product _model is  our ORM object.
 */
$product = $product_model->find_by_id($_REQUEST['product_id']);

# Include the common header for all the site's pages.
include('templates/header.php');
?>
<!-- This is your site's add to cart button -->
<button id="add_to_cart">Add to Cart</button>

<script type="text/javascript">
  $('#add_to_cart').click(function(e) {    
  _funnely_q.push(['trackAddToCart', {product_ids: [<?= $product->id ?>], product_price: <?= $product->price ?>, product_currency: '<?= $product->currency ?>', content_name: '<?= $product->name ?>'}])
  });
</script>
<?php
# Include the common footer for all the site's pages
include('templates/footer.php');
?>
```

### 2.5. Checkout Tracking ###

In order to track the completion of the order process, you need to place the following code in the 'Order Placed' (also known as checkout or 'thank you' page) of your site.


```
#!javascript
  _funnely_q.push(['trackCheckout', {product_ids: [<product-id-1>, <product-id-2>, ...], order_amount: <order_amount>, order_currency: <order_currency>, num_items: <num_items>, content_name: <content_name> }])
```

The trackCheckout function takes five parameters:


* **order_amount**: a float that is the total amount of the order. This field is mandatory.
* **order_currency**: a string that represents the currency for the order amount in ISO 4217 format. This field is mandatory.

The following fields are only used if you are going to use Facebook's Dynamic Product Ads. Otherwise, you must not use them:

* **product_ids**: an array with the ids of the products included in the purchase. This field is mandatory **only** if you are going to use Dynamic Product Ads.
* **num_items**: The number of items in the cart. This field is optional.
* **content_name**: is a string with the name of the products added to the cart. This field is optional.


As with the previous steps, you can set these values in the template where you include this code, using your site's business logic. Here's an example in PHP:


```
#!php
<?php

/**
 * We assume that the $order variable represents the model for the order that is being retrieved, 
 * and $order_model is  our ORM object.
 */
$order = $order_model->find_by_id($_REQUEST['order_id']);

# Include the common header for all the site's pages.
include('templates/header.php');
?>
<!-- Here's your order page content -->
<script type="text/javascript">
  _funnely_q.push(['trackCheckout', {product_ids: [<?= $order->product_ids ?>], order_amount: <?= $order->amount ?>, order_currency: '<?= $order->currency ?>', num_items:  <?= $order->num_items ?>, content_name:  '<?= $order->content_name?>'}])
</script>
<?php
# Include the common footer for all the site's pages
include('templates/footer.php');
?>
```
### 2.6. Subscription Tracking ###

In order to track the event of a customer subscription (i.e. to your site or newsfeed), you need to place the following code either in the subscription confirmation page, or capture the 'subscribe' button click.

```
#!javascript
  _funnely_q.push(['trackSubscription', {subscriptor_id: <subscriptor_id>, subscriptor_name: <subscriptor_name>, subscriptor_email: <subscriptor_email>}])
```

The trackSubscription function takes four parameters:

* **subscriptor_id**: the subscriptor's internal identifier in your system (if this is not available, you can use an empty string)
* **subscriptor_name**: the subscriptor's name (can be an empty string if no name is required in your form)
* **subscriptor_email**: the subscriptor's email

As with the previous steps, you can set these values in the template where you include this code, using your site's business logic. Here's an example in PHP:

```
#!php
<?php

/**
 * We assume that the $subscriptor variable represents the model for the customer that has just subscripted to your site.
 * This code would be placed in your 'thanks for subscribing' page.
 * and $order_model is  our ORM object.
 */
$subscriptor = $subscriptor_model->find_by_id($_REQUEST['subscriptor_id']);

# Include the common header for all the site's pages.
include('templates/header.php');
?>
<!-- Here's your order page content -->
<script type="text/javascript">
  _funnely_q.push(['trackSubscription', {subscriptor_id: <?= $subscriptor->id ?>, subscriptor_name: <?= $subscriptor->name ?>, subscriptor_email: '<?= $subscriptor->email ?>'}])
</script>
<?php
# Include the common footer for all the site's pages
include('templates/footer.php');
?>
```

## 3. Verifying your implementation ##

In order to verify your Funne.ly tracking API implementation, from your browser developer console you must run the following command:

```
#!javascript
funnely.diagnostics()
```
A common output for this command is

![console_output_diagnostics.png](https://bitbucket.org/repo/RnG76r/images/3695678252-console_output_diagnostics.png)

Notice that the tracking types that have been activated are green, while the ones that haven't been activated are red. The activated trackings will also display the last time the tracking was called.

There are a few reasons why tracking might not be working:

1. The javascript code for tracking a section isn't placed correctly. Please note that in order to get the activated status, you must visit the section, otherwise the script will not be called and the activation will not occur.

2. For CategoryVisit and ProductVisit, make sure that the data that the parameters that you have set match the data in your category and product feeds. If you send the wrong unique id, or the wrong category type, the category that you're visiting will not match with our feed, and the tracking will not happen.

3. For AddToCart, make sure that the code for tracking the add to cart action is executed when you click the 'Add to Cart' button in your site.

4. For Checkout, make sure that you're sending the order parameters requested by the trackCheckout() method, and that you visit the page where you have placed this code, before checking its activation status.

## 4. Creating your Product Catalog for Dynamic Product Ads ##

There are two requirements for accessing the Dynamic Product Ads features in Funne.ly:
###1. Have a Business Account created and set up in your Funne.ly configuration###

If you don't have a Business Account, go to ![Facebook Business Manager[(http://business.facebook.com/). Setting up a Business Account is easy, and it has valuable features besides the ability to create Dynamic Product Ads such as 3rd party access control, multiple ads account management, etc.

###2. Create a Product Catalog to let Funne.ly manage your product catalog within your Facebook Business###

Facebook's Dynamic Product Ads use your product information in order to dynamically create Ads that are used to retarget potential customers that have visited your product pages.
In order for Facebook to know about your product characteristics, a Product catalog feed is requested.

The format for the content of this feed is TSV (Tab Separated Value), and the following is a list of fields that is required for each of the products in your catalog:

![product_catalog_mandatory_fields.png](https://bitbucket.org/repo/RnG76r/images/802253029-product_catalog_mandatory_fields.png)

There is an optional, but useful field if you want to do retargeting at the Category level that is the 'product_type' field. This field represents the name of the Category that your product belongs to.

![Product Catalog Setup.png](https://bitbucket.org/repo/RnG76r/images/3719475153-Product%20Catalog%20Setup.png)

Here is a sample of the product feed that you can use. *Make sure to delete the sample product!*
[Sample Product Catalog File](https://bitbucket.org/funnely/funnely-tracking/raw/8d8b7f6241e596b83aa3ebabe63390643f09bc99/funnely_product_catalog_sample.tsv?at=master)

If you want to send your catalog in a different format (XML, CSV, etc) please look at Facebook's [Product Catalog Feed](https://developers.facebook.com/docs/marketing-api/dynamic-product-ads/product-catalog) documentation

If you have any questions while creating your catalog feed, contact us at setup@funne.ly, so we can set your Dynamic Product Ads.

# Shopify Cheat Sheet #

In the **theme.liquid**, within the <head></head> tag:

```
// Copy the main script from your Funnely account Settings page
```

In the **index.liquid** template, before the </body> closing tag:

```
<script type="text/javascript">
  _funnely_q.push(['trackHomepageVisit']);
</script>
```

In the **collection.liquid** template, before the </body> closing tag:

```
<script type="text/javascript">
_funnely_q.push(['trackCategoryVisit',{category_id:'{{ collection.id }}',category_type:'{{ collection.title }}'}]);
</script>
```

In the **product.liquid** template, before the </body> closing tag:

**IMPORTANT**: *The product id must match the id of the product in your product feed. In this example, we're using '{{ product.variants.first.id }}'. Sometimes, depending of what you are using to generate the feed, it could be different. In case your feed looks like this:*

```
...
<item>
  <g:id>shopify_435985909_1149688041</g:id>
  <g:title>14 Day Nutrition Guide</g:title>
...
```
*you should replace '{{ product.variants.first.id }}' with  'shopify_{{product.id}}_{{product.variants.first.id}}'*.

```
<script type="text/javascript">
// Insert Your Add-To-Cart Button's ID below.
    $('#addToCart').bind( 'click', function(){
      _funnely_q.push(['trackAddToCart', {product_ids: ['{{ product.variants.first.id }}'], product_price: '{{ product.price | money_without_currency }}', product_currency: '{{ shop.currency }}', content_name: "{{ product.title }}"}]);
    });
               
    _funnely_q.push(['trackProductVisit', {product_id: '{{ product.variants.first.id }}', product_price: '{{ product.price | money_without_currency }}', product_currency: '{{ shop.currency }}'}])
</script>
```

And finally, in **Settings > Checkout**, in the box to insert code to be executed at checkout:
**IMPORTANT 1**: *you need to also include the Funnely main code (the one you can find in the **Settings** section of your Funnely account)*

**IMPORTANT 2**: *The product id must match the id of the product in your product feed. In this example, we're using '{{ line_item.variants.id }}'. Sometimes, depending of what you are using to generate the feed, it could be different. In case your feed looks like this:*

```
...
<item>
  <g:id>shopify_435985909_1149688041</g:id>
  <g:title>14 Day Nutrition Guide</g:title>
...
```
*you should replace '{{ line_item.variant.id }}' with  'shopify_{{line_item.product.id}}_{{line_item.variant.id}}'*.

```
<script type="text/javascript">
var fnly_items = []; 
var fnly_order_amount = {{ total_price | money_without_currency }}; 
var fnly_order_currency = "{{ settings.default_currency | default: shop.currency }}"; 
var fnly_num_items = 0; 

{% for line_item in line_items %} 
  fnly_items.push('{{ line_item.variant.id }}'); 
  fnly_num_items += {{ line_item.quantity }}; 
{% endfor %} 
  _funnely_q.push(['trackCheckout', {product_ids: fnly_items, order_amount: fnly_order_amount, order_currency: fnly_order_currency, num_items:  fnly_num_items, content_name:  'order'}])
</script>
```